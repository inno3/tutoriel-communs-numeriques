# #Introduction à l'atelier sur le financement des communs

##Présentation de Decidim -- A. Gaboriau 

Decidim est un logiciel né à Barcelone en 2015/2016, impulsé par une liste citoyenne élue en 2015 à la mairie, avec la volonté politque de développer un outil pour faire participer les gens aux décisions municipales. L'objectif de Decidim est d'intégrer les différentes dynamiques de participation citoyenne dans un seul outil : un espace "assemblée"; un espace de pétition et de vote associé; un espace pour recenser les initiatives.

L'équipe produit est basé à Barcelone et compte 10 membres, il y a ensuite un groupe de coordination municipale, un groupe de chercheurs qui travaillent sur des sujets de fond (e.g. comment intégrer la Blockchain?), un laboratoire dédié aux citoyenss et une plateforme dédiée à la proposition de nouvelles fonctionnalités. Il y a également des participants extérieurs (OSP). Decidim ne fait pas que fournir la plateforme, elle a aussi une mission de conseil, de développement de fonctionnalités légères.

Il y a également tout un ecosystème de prestataires informatiques en Espagne permettant de réaliser des développements plus lourds, ce qui a permisd'aboutir à un outil fonctionnel, souple et assez facile d'utilisation. Il y a des versions intermédiaires qui sortent tous les mois,

###Pourquoi avoir développé / soutenu 2 outils Open Source ?

Democratie OS est plus dédié à du "one-Shot", processus légers. Par exemple : vote en ligne pendant une AG associative

Decidim est plus axé participation à long-terme, avec beaucoup de participants. Par exemple : budget participatif dans une commune.

Les deux sont donc complémentaires

La mutualisation de tous ces acteurs est assez compliquée, il faut donc des outils tels que la plateforme virtuelle évoquée précédement.

Pistes d'évolution du commun / sources de financement

En prévision des prochaines élections à Barcelone (2019)

financement européen : pas obtenu

Idée de créer une entité juridique européenne pour Decidim

Fonds de dotation, alimenté par l'Etat ou les CT

En Belgique, négociation d'un marché avec l'Etat belge qui offrirait ensuite un accès à la plateforme aux municipalités. Conditions de cet accord ? L'Etat belge héberge une version mère, et chaque fois qu'une municipalité voudra ouvrir une plateforme, elle demandera un accès à l'Etat sans passer par Decidim.

[OpenFoodFrance] on réfléchit avec différentes structures à comment organiser de grosses SCIC pour assurer la prise de décision démocratique, surtout que l'on est en train de se fédérer. [Decidim] on a été contacté par des structures similaires, ça serait une piste intéressante pour Decidim

[eQuartier] Est ce que dans l'état actuel le logiciel peut répondre à des besoins de gouvernance distribuée basée sur des communauté locales (a l'échelle du quartier ou de la ville moyenne soit quelques centaines de membres contributeurs) ? [Decidim] il pourrait oui.

##Présentation d'Anthropotech -- C.Bazin -- 10'

Développer une Economie Sociale et Solidaire autour des communs

Le financement est notre question de base, on s'est demandé en 1er lieu qui aurait usage du projet ? L'économie sociale et solidaire a été une première piste en tant qu'alternative à l'économie

Hypothèses :
* les communs sont importants
* il y a un besoin de trouver de nouveaux modes de financement, mieux adaptés aux communs.
* Nécessité de prendre en compte l'importance des plateformes.

Point de départ sur la question du financement (question jugée prioritaire) : Est-ce qu'il n'y aurait pas possibilité de monter un fonds de soutien alimenté par les acteurs de l'ESS (certains étant riches et/ou ayant obligation de distribuer)

Ensuite, promotion de l'idée de résidence, travailler ensemble avec les usagers finaux pendant une longue période (pour désamorcer un raisonnement qui résumerait les acteurs à leurs seuls pb).

On pense qu'un fonds de soutien, un financement de départ, ce n'est pas suffisant, il faut de quoi maintenir le logiciel. On a donc pensé à un infuseur numérique : des porteurs de projets vont s'inscrire dans la démarche et seront accompagnés. Importance de l'économie des communs, exemple du festival de Clermont-Ferrand, qui a mis en place un site pour s'inscrire, puis pour déposer son film, etc. D'abord pour eux, puis ils l'ont proposé à d'autres festivals à un prix modeste mais permettant d'assurer les futurs développements.

C'est une piste, avec aussi les licences à réciprocité, l'indépendance des commoners (travail à côté des communs), elle peut trouver sa réponse soit dans l'ESS soit avec les licences à réciprocité (manière de s'interfacer avec un marché)

Il faut aussi penser à l'indépendance financière des commoners : créer des produits et services pour le marché pour que l'argent qui est collecté par cette activité finance les communs. "Hacker l'économie cynique et suicidaire pour tranférer la ressource vers l'économqie sociale et solidaire" (Michel Bauwens)

###Réactions

[Matty Schneider] **Qui serait intéressé pour financer les communs (notamment à l'échelle politique) ?**

Je trouve surprenant d'aller chercher des acteurs extérieurs juste intéressés pour le financement de l'idée politique, car on rend le commun dépendant de l'extérieur.

**Peut-on trouver des sources de financement à l'intérieur ?**

Si on fonde le financement sur une idée politique, ça me parait compliqué.

[C.Bazin (Anthropotech)] ça resterait une force d'appui, mais pas de leadership. L'idée de résidence est intéressante mais présente un risque de prise de contrôle sur le commun par une core team
L'idée est de faire discuter les usagers finaux pour enclencher la communauté au-delà des seuls développeurs. Créer des communautés qui voient l'utilité du projet

i
**Il faut une structure qui porte le commun, mais pourquoi ?**

[C.Bazin] c'est effectivement compliqué, l'idée d'avoir une structure spécifique au commun permet de le régler et de le maintenir dans le temps en dehors des autres structures. La coopérative parait être la plus intéressante.

[M.Schneider] Une structure de droit moral peut paraître antinomique avec les communs : une entité versus pluralité de participants.

[Julien Lecaille (Coop des communs)] identifie une problématique d'arbitrage entre contrôle et lâcher prise, car l'ESS est marquée par les valeurs quand le libre laisse filer : priorité aux valeurs ou aux pratiques ? Communauté cloisonnée ou distribution complète => 2 axes : * priorité aux valeurs / priorité aux pratiques * une communauté cloisonnée / communauté distribuée

Pas de dogme, mais chaque modèle économique doit être précisément situé sur ces axes, même s'il peut évoluer

[Applicolis] Séparation entre le commun numérique et l'activité économique localisée. Idéalement l'activité économique doit pouvoir exister sans les communs. Le commun a un rôle critique dans l'acceleration du démarrage et la diminution de la prise de risque.

Il faut séparer les projets productifs s'inscrivant dans une économie locale, ayant pour ambition de faire vivre leur travailleurs (dans le cas d'Applicolis, de eQuartier, d'Happy Dev), du commun qui centralise les fonctions supports et de mise en place d'outils de facilitation de l'activité économiques des projets locaux.

Le commun peut avoir une activité économique propre comme de l'apport d'affaire pour une communauté identifiée par une charte par exemple (livraison 100% écologique et humainement responsable dans le cas d'Applicolis). De cette manière le commun est en parti autofinancé grâce à la force de sa communauté et contribue à injecter de la ressource dans ces membres, renforçant donc le commun. L'outil juridique réduisant le risque de dépendance est la SCIC. Ce sont les structures locales qui font parties du collège majoritaire de gouvernance de la SCIC.

**Dans quelle mesure l'argent peut-il tuer la contribution ?**

##Présentation d'Open Law -- S. Saint Auguste

Open Law - association de soft law qui promeut l'innovation ouverte dans le secteur du droit (cloisonné, réglementé, concurrentiel) et produit des ressources numériques ouvertes (Open Data, Open Source, Open...) 3 ans d'existence, objectif premier de s'intéresser à l'open data puis élargissement à toutes les problématiques juridiques. Particularité : repose sur une association inédite d'acteurs privés et publics. À l'intersection de secteurs aux intérêts divergents, et porteuse d'une contradiction : porteuse de communs mais pas reconnue d'IG (intérêt général) car simplement la somme des intérêts particuliers de ses membres. => Recours à un fonds de dotation

Open Source / Bien Commun / Intérêt Général : le fonds de dotation est la structure qui a permis de définir un cadre pour prétendre à l'intérêt général.

Le fonds de dotation permet également la défiscalisation + mécénat de compétences.

Le fonds de dotation est financé par l'association Open Law, grâce aux personnes morales de l'association et cotisations (10€ p.physique jusqu'à 15.000€ p.morale)

Le fonds de dotation comporte 1 seul membre (Open Law), mais à terme, il pourrait s'ouvrir pour en accueillir d'autres pour faire du commun.

###Réactions

[M.Schneider] Je suis surpris de cette notion d'IG de l'administration fiscale, qui se rajoute aux intérets des autres membres. Pour obtenir la défiscalisation il faut respecter un cahier des charges pour être reconnu d'IG par l'admin fiscale.

[OpenFoodFrance] quels liens entre OpenLaw et ShareLex ? Votre modèle repose sur le fait que ses membres peuvent financer, ce qui n'est pas le cas de tous. ShareLex : outil soutenu par OpenLaw

[Boris Séguy] Risque : Difficulté de trouver des financements :
* Travailler plus pour atteindre une maturité qui permet de s'autofinancer; plutôt que chercher des financements
* Soit autofinancement par l'activité économique du communs, soit nécessité des financements publics

[FING] Le fonds de dotation est déjà un progrès en terme de facilité d'activation (par rapport au statut de fondation) Sans opposer les solutions, on peut toutefois explorer comment les acteurs de l'ESS pourraient contribuer même de façon très marginale (mais en France nous avons le nombre) Les associations ne sont pas contributrices alors qu'elles tirent les fruits de communs informationnels (logiciels open source, informations non payantes, méthodes d'animation libres, ...). La vision stratégique du numérique est naissante dans l'ESS d'où cet impensé : la gouvernance

**Création monétaire par les communs ?**