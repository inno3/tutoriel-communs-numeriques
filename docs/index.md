![NEC](images/LogoNEC_siteweb.png) 

*Find an English version of this Introduction [here](https://vbachelet.frama.io/tutoriel-communs-numeriques/index_eng/)*

**Bienvenue sur le site du projet « Tutoriel aux communs numériques ». Il s'agit d'une initiative impulsée par la [mission Société Numérique](https://societenumerique.gouv.fr/) (de l'Agence du numérique) et la [MedNum](https://lamednum.coop/) et coordonnée par [Inno³](https://inno3.fr).**

**La version actuelle du tutoriel mise à disposition sur ce site résulte d'un travail collaboratif de plusieurs fois, mêlant contributions numériques et physiques.** 

## Un commun numérique ?

Un commun désigne **une ressource produite et/ou entretenue collectivement par une communauté d’acteurs hétérogènes, et gouvernée par des règles établies démocratiquement qui lui assurent son caractère collectif et partagé.** 

Il est dit numérique lorsque la ressource est numérique : logiciel, base de données, contenu numérique (texte, image, vidéo et/ou son), etc. Il est ainsi possible de parler de communs numériques en présence de bases de données Open Data coproduites et gouvernées collectivement, de Logiciels Libres/Open Source ou de contenus ouverts répondant aux mêmes règles.

## Pourquoi ce tutoriel ?

Pour **répondre à un besoin croissant d’acteurs, publics mais également privés, qui souhaitent initier des démarches de communs numériques** (soit pour ouvrir des ressources qu’ils développent ou détiennent, soit pour contribuer à des ressources tierces pré existantes ou à construire, etc.) et qui cherchent un accompagnement.

Il s’agit donc d’apporter des pistes réponses concrètes à leurs interrogations, de lever peu à peu les barrières culturelles, techniques, organisationnelles, économiques souvent associées aux démarches d’ouverture et de mutualisation. 

Ce travail prend la forme d'un tutoriel **générique**, **instanciable** pour des besoins spécifiques, auquel sont associés différents **référentiels** en appui à cette démarche d'harmonisation et de vulgarisation.

Premières instanciations : 
* Par la [Mission Société Numérique](https://societenumerique.github.io/tutoriel)), à destination des collectivités territoriales 
* Par l'association Open Law, Le Droit Ouvert : en cours

## Un tutoriel pour qui ?

Ce tutoriel s’adresse à **toutes celles et tous ceux qui souhaitent développer un projet de commun numérique, ainsi qu’à toutes les personnes – publiques comme privées – impliquées dans le maintien ou le développement de tels projets**.

## Notre démarche

### Numérique en Commun[s]

Cette démarche a été initiée à l’occasion de [Numérique en Communs](http://numerique-en-commun.fr/) à Nantes en septembre dernier, dans le cadre du Parcours « Communs Numériques » composé de 5 ateliers répartis sur les 2 jours de l’événement. 

Ces ateliers ont donné lieu à des comptes-rendus disponibles sur le [site de NEC](https://pad.numerique-en-commun.fr/parcours-communs#) ainsi qu'une série de référentiels initiés, en support au tutoriel et dans la logique même de constitution de communs, dans l'idée d'accompagner concrètement la mise en œuvre des principes contenus dans le tutoriel. 

### Les ateliers

La réflexion s'est ensuite poursuivie au cours d'un [meet-up organisé à la Maison du Libre et des Communs en octobre dernier](https://inno3.fr/evenements/meetup-contributeurs-du-parcours-communs-numeriques), l'occasion d'un temps d'échange permettant de dresser une première liste des questions autour desquelles structurer le tutoriel final.

Dans le prolongement de cette démarche, une [soirée d'échange a été organisée à Bercy fin 2018](https://inno3.fr/evenements/soiree-de-presentation-du-tutoriel-communs-numeriques-le-1912-18h) afin de présenter la première version du tutoriel pour les démarches de communs numériques (questions, réponses, ressources et exemples) et de préfigurer les prochaines étapes de ce parcours collectif. 

### La suite 

Ces premiers travaux ont à présent vocation à être perfectionnés et/ou complétés par d'autres ressources et/ou contributeurs, dans une démarche de plus long terme, ouverte et collaborative afin de bâtir ainsi un état de l'art solide.

En parallèle, le tutoriel ainsi mis à disposition a vocation à être saisi par tout type de projet, organisation ou acteur souhaitant soutenir une démarche de communs numériques. Pour cela, n'hésitez pas à l'implémenter directement en l'adaptant à vos besoins et spécificités. 


## Les contributeurs

### Contributeurs actuels

Retrouver la liste des contributeurs actuels du projet [ici](https://vbachelet.frama.io/tutoriel-communs-numeriques/contributors/)

### Pour contribuer

Toutes les ressources du projet sont disponibles sur la [plateforme Framagit](https://framagit.org/inno3/tutoriel-communs-numeriques). 

Par ailleurs, n'hésitez pas à nous contacter pour venir contribuer au projet (nec@inno3.fr) et/ou à vous abonner à la [liste de diffusion du projet](https://framalistes.org/sympa/info/tutoriel-communs-numeriques).
