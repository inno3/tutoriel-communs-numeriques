#Introduction à l'atelier sur l'identification des ressources

Vous trouverez ici un résumé ainsi que les supports de présentation des interventions liées à l'identifation des ressources susceptibles d'être gérées en commun. 

##Présentation du Livre Blanc "Construire des communs numériques" par  Matti Schneider

Matti Schneider a écrit un livre blanc pour aider celles et ceux qui s'engageraient dans une telle démarche à garantir un usage libre, juste et pérenne des outils logiciels qui contribuent au bien commun.

[Billet de Matti Schneider présentant son Livre Blanc](https://medium.com/@matti_sg_fr/dessine-moi-un-commun-num%C3%A9rique-8d921451be31)

[Le Livre Blanc](https://communs.mattischneider.fr/)

[Support de présentation](https://speakerdeck.com/mattisg/construire-des-communs-numeriques)

##Méthodologie de la Fabrique des mobilités

http://wiki.lafabriquedesmobilites.fr

L’appel à communs fonctionne sous forme de défis, chacun peut proposer des défis, et ceux qui rassemblent le plus de voix peuvent se voir accompagner pour devenir des communs.

##Méthodologie d’Open Law

openlaw.fr

L’enjeu est à la fois de favoriser l’émergence de communs au sein de l’association, mais également de sensibiliser des acteurs extérieurs Système de fonds de dotation permettant de collecter largement et de prioriser les sujets sur lesquels on va produire des ressources. Ce fonds est une personne morale qui ne peut financer que des projets d'intérêt général. Le premier commun à mettre en place est celui des normes et standards.
