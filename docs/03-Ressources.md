# Ressources

## Référentiels

Initiés en support au tutoriel et dans la logique même de constitution de communs, ces référentiels ont pour objectif d'accompagner concrètement la mise en œuvre des principes contenus dans le tutoriel. 

Ces premiers travaux ont à présent vocation à être perfectionnés et/ou complétés par d'autres ressources et/ou contributeurs, dans une démarche ouverte et de long terme. 

### Identifier

* [Grille d’opportunité pour la contribution](https://framagit.org/inno3/tutoriel-communs-numeriques/blob/master/referentiels/1_Grille_OpportuniteContribution.pdf) : référentiel coconstruit listant un certain nombre de facteurs susceptible de justifier et rationaliser la contribution à un commun numérique.

* [Grille d’opportunité pour la conception d’un commun](https://framagit.org/inno3/tutoriel-communs-numeriques/blob/master/referentiels/1_Grille_OpportuniteOuverture.pdf) : référentiel coconstruit listant un certain nombre de facteurs susceptibles de justifier et rationaliser une démarche de conception de commun numérique.

### Produire 

* [Liste des besoins fréquents d’un commun](https://framagit.org/inno3/tutoriel-communs-numeriques/blob/master/referentiels/2_Tableau_OrganiserLaContribution.pdf) : référentiel coconstruit listant un certain nombre de besoins fréquents formalisés au sein de projets de commun numérique.

### Maintenir - pérenniser

* [Piliers & lignes directrices pour favoriser la pérennité d’un commun](https://framagit.org/inno3/tutoriel-communs-numeriques/blob/master/referentiels/3_PiliersPerenniteCommuns.pdf) : exemples d'éléments clé ayant vocation à donner confiance l’ensemble des parties prenantes en leur fournissant un environnement sécurisant et propice à la création.

### Financer 

* [Liste des besoins de financement et dispositifs associés](https://framagit.org/inno3/tutoriel-communs-numeriques/blob/master/referentiels/4_Panorama_FinancementsActeursModalites.ods) : référentiel coconstruit listant les besoins de financement d’un commun numérique et des dispositifs existants, par typologie de financeurs et de besoins.

### Disséminer 

* [Liste d’argumentaires](https://framagit.org/inno3/tutoriel-communs-numeriques/blob/master/referentiels/5_Argumentaires_DisseminationCommuns.pdf) : référentiel coconstruit listant les besoins un certain nombre d’arguments susceptibles d’être mis en avant afin de communiquer sur le caractère particulier du commun et les avantages associés.


## Ressources diverses

En complément des référentiels, il existe de nombreuses ressources utiles dans le cadre de la contribution et/ou de la création d'un commun numérique. Cette liste n'est pas exhaustive et à vocation à évoluer librement. 

* [Veni, Vidi, Libri](vvlibri.org/fr) : le projet de l’association Veni Vedi Libri répond aux principales questions rencontrées par des communs numériques en matière de licences libres. 

* [La « politique » Framabook et les licences libres (par C. Masutti et B. Jean)](https://inno3.fr/la-politique-framabook-et-les-licences-libres-par-c-masutti-et-b-jean) : document détaillant l’articulation du régime des licences, en matière de gestion de la propriété intellectuelle, et des différents usages susceptibles d’être sanctionnés sur la base d’un autre fondement.

* [Méthodologie pour définir le cadre de collaboration d’un projet](https://framagit.org/inno3/tutoriel-communs-numeriques/blob/master/ressources/M%C3%A9thodologie%20SpeedBoat%20-%20Cadre%20de%20collaboration.pdf) : méthodologie pour la conduite d’un exercice collectif dit «du speedboat», qui a pour objectif de lister les leviers, freins et obstacles rencontrés le projet ou qui peuvent se présenter à lui. Cet exercice permet également d’identifier les éléments structurants les plus adaptés pour tirer profit des leviers et anticiper les freins et obstacles potentiels. 

* [Exemples de structurations juridiques constituées autour de communs numériques](https://framagit.org/inno3/tutoriel-communs-numeriques/blob/master/ressources/Exemples_StructuresJuridiques_Communs.pdf) : tableau synthétique basé sur les projets détaillés en atelier permettant de présenter les différentes structurations juridiques formalisées.

* [Exemples de gouvernance : exemples des cadres de collaboration](https://framagit.org/inno3/tutoriel-communs-numeriques/blob/master/ressources/ExemplesChartes-CadresCollaboration.pdf)  rédigés par des projets issus de secteurs différents : Fédération Open Space Makers (secteur spatial), Fabrique des mobilités (secteur des transports & mobilités), LF Energy (secteur de l'énergie). 

