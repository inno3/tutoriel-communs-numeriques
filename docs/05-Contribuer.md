# Contribuez !


## Contributeurs actuels

Retrouvez la liste des contributeur.ices actuel.les du projet  [ici](https://vbachelet.frama.io/tutoriel-communs-numeriques/contributors/).

## Pour contribuer

Toutes les ressources du projet sont disponibles sur la [plateforme Framagit](https://framagit.org/inno3/tutoriel-communs-numeriques). 
Si vous ne connaissez pas GitLab, nous vous invitons à consulter la [documentation proposée par Framasoft](https://docs.framasoft.org/fr/gitlab/).

Par ailleurs, n'hésitez pas à nous contacter pour venir contribuer au projet (nec@inno3.fr) et/ou à vous abonner à la [liste de diffusion du projet](https://framalistes.org/sympa/info/tutoriel-communs-numeriques).
