# Exemples

##Open Food Facts 

Date de création : 2012 

###Présentation de la ressource

Open Food Facts est une base de données relative aux produits alimentaires. 

Elle répertorie les ingrédients, les allergènes, la composition nutritionnelle et toutes les informations présentes sur les étiquettes des aliments. Elle permet notamment d’avoir accès à la composition d’un produit et à son Nutri-Score, informant sur la qualité nutritionnelle du produit, simplement en scannant son code-barre. 

Ces données sont renseignées par des contributeurs (plus de 9000) répartis dans le monde entier. Considérées comme étant d'intérêt public, elles sont ouvertes et peuvent être utilisées par tous et pour tous usages.

[Découvrir Open Food Facts](https://fr.openfoodfacts.org)

###Communauté & contributions

La communauté d'Open Food Facts se compose en grande partie des contributeurs directs qui alimentent la base de données. Des consommateurs du monde entier aident ainsi à enrichir la base de données en photographiant les étiquettes des produits.
[Page contributeurs d'Open Food Facts](https://fr.openfoodfacts.org/contributeurs)

Les contributeurs interviennent également sur d'autres besoins, nécessaires au développement et à pérennisation de la ressource : 

   * Communication \& dissémination du projet : faire connaître le projet, faciliter son adaptation à l'international (traductions, etc.). 
   * Développement : serveur, site, applications mobiles
   * Construction de la communauté 
   * Design 
   * Gestion de projet 
   * Etc. 

###Gouvernance

La gouvernance est pilotée par une association éponyme, loi 1901 à but non lucratif, qui coordonne le projet Open Food Facts et garantit sa pérennité technique, juridique, économique, etc. ainsi que son indépendance. 

Le projet est financé principalement par les dons, notamment grâce à des [campagnes de dons](https://www.helloasso.com/associations/open-food-facts/collectes/aidez-open-food-facts-a-ameliorer-l-alimentation-de-tous). 

La prise de décisions implique tous les contributeurs intéressés, au-delà des membres de l’association, qui échangent sur Slack. Plusieurs équipes thématiques se sont constituées (développement, communication, etc.). 

###Licences utilisées

   * La base de données Open Food Facts est disponible sous la licence Open Database License (ODbL).
   * Les contenus individuels de la base de données sont disponibles sous la licence Database Contents License.
   * Les photos de produits sont disponibles sous la licence Creative Commons Attribution Partage à l'identique. Elles peuvent contenir des éléments graphiques soumis à droit d'auteur, copyright ou droit des marques, qui peuvent dans certains cas être reproduits (droit de citation, droit à l'information ou "fair use"). 

###Exemples d’utilisation

La base de données Open Food Facts est réutilisée gratuitement par plus de 100 applications mobiles et services web. Elle est également utilisée par des équipes de recherche en santé publique, à l'instar de l'EREN (Équipe de Recherche en Épidémiologie Nutritionnelle) qui a utilisé les données pour publier des articles scientifiques. 

##OpenENT

Date de création : 2014

###Présentation de la ressource

Open ENT est une solution open source destinée aux établissements d’éducation secondaire, afin de fournir aux élèves un espace numérique de travail sur lequel il peuvent retrouver toutes les informations en lien avec leur scolarité et accomplir un certain nombre de démarches. 

###Licences utilisées

Le [code source](https://open-ent-ng.github.io) est distribué sous licence AGPL. 

###Communauté

La communauté qui contribue à OpenENT se compose d'acteurs publics et privés (collectivités territoriales, dont la Ville de Paris, les départements du Poitou-Charente, Picardie, Essonne, Seine et Marne, d'éditeurs logiciels, d'entreprises, etc.). 

Un [groupe de discussion](https://groups.google.com/forum/?hl=fr#}!forum/open-ent-ng) permet aux utilisateurs et contributeurs d'échanger, poser des questions, etc.

###Gouvernance

La gouvernance d’Open ENT est assurée par plusieurs collectivités territoriales, qui se sont regroupées dans le cadre d'une association afin de coordonner collectivement le développement d'Open ENT (identification des fonctionnalités à développer, financement, condition de partage, etc.). Dans les faits, chaque collectivité utilisatrice du projet peut bénéficier des développements réalisés, tout en s'engageant à son tour à reverser les développements dont elle est à l'initiative et qu'elle aura financés pour répondre à ses besoins spécifiques. 

###Exemples d’utilisation

La Ville de Paris s’est basée sur le projet Open ENT pour développer sa plateforme Paris Classe Numérique – Nouvelle Génération. L’objectif étant que, de la maternelle au lycée, les élèves parisiens puissent suivre, avec leurs familles, l’ensemble de leur scolarité dans le même environnement numérique, avec les mêmes identifiants.


##Decidim

Date de création : 2017 

###Présentation de la ressource

Decidim est une plateforme numérique de participation citoyenne pour une ville démocratique, construite de manière ouverte et collaborative à l’aide de logiciels libres. Au-delà des collectivités, Decidim est également utilisée pour favoriser la participation de la gouvernance démocratique de projets ou d'organisations telles que des associations. 

###Licences utilisées

Le [code source](https://github.com/decidim/decidim/blob/master/LICENSE-AGPLv3.txt) de la plateforme est disponible sur GitHub sous licence AGPLv3.

L’ensemble des autres contenus relevant du droit d’auteur est placé sous licence CC-BY-SA 4.0.

Les données produites par la plateforme seront quant à elles publiées sous licence ODbL

###Communauté

La communauté de Decidim se compose d’utilisateurs et de contributeurs hétérogènes, comprenant aussi bien des citoyens sans compétences particulières en matière de développement, des développeurs, des chercheurs et des institutions, dont notamment la mairie de Barcelone, première et principale utilisatrice de Decidim. 

Chaque contributeur est invité à participer au projet selon ses compétences et son temps disponible. 

[Page "Communauté" de Decidim](https://decidim.org/community/)

###Gouvernance

La gouvernance du projet est assurée au travers de MetaDecidim, une communauté regroupant les contributeurs désireux de prendre part à la gouvernance du projet. 

La communauté se structure en plusieurs groupes de travail axé sur une problématique donnée, le tout s’inscrivant dans une feuille de route décidée collectivement. 

La communication et la prise de décision passe notamment par le recours à la plateforme Decidim, ainsi que par d’autres réseaux sociaux. 

###Exemple d’utilisation

L’exemple qu’a décidé d’incarner la municipalité de Barcelone a fait florès puisqu’aujourd’hui Decidim est utilisé par de nombreuses collectivités à travers le monde ; mais également par des structures de droit privé.

Parmi les collectivités s’appuyant sur Decidim, le département du Loiret a développé une plateforme dénommée L’atelier de vos idées permettant de mener des consultations publiques auprès des Loirétains. La première consultation, en cours actuellement (novembre 2018) offre la possibilité aux citoyens de prendre part à l’allocation d’un budget de 80 000€ destiné à la mise en valeur des 7 parcs naturels départementaux, en votant parmi 32 projets. 


##OpenStreetMap

Date de création : 2004

###Présentation de la ressource

OpenStreetMap est un projet de cartographie libre et ouvert visant à constituer de manière collaborative une base de données géographiques en Open Data afin de permettre la création de cartes sous licences libres. 

Découvrir le projet [OpenStreetMap](https://www.openstreetmap.org/) 

###Licences utilisées

La base de données ainsi constituée est placée sous l’Open Database License (ODbL) d’Open Data Commons ; tandis que les rendus cartographiques produits directement par OpenStreetMap et la documentation sont placés sous CC-BY-SA 2.0. 

###Communauté

Le projet revendique plus d'un million de contributeurs à travers le monde. Ces contributeurs sont en premier lieu des personnes physiques, mais également des personnes morales. Parmi ces personnes morales figurent des collectivités territoriales (la Ville de Vienne, etc.) et des entreprises (Michelin).  Certaines données sont également issues de services publics d’accès aux données, notamment cartographiques (PSMA Australia, etc.).

###Gouvernance

La [gouvernance](https://wiki.osmfoundation.org/wiki/Main\_Page) du projet et de la communauté est assurée au nom de cette dernière par la Fondation OSM. La Fondation est constituée de membres répartis entre 3 catégories, à jour de cotisation, qui élisent à leur tour un bureau de 7 membres. Les membres peuvent également rejoindre un des nombreux goupes de travail, chacun consacré à une problématique précise. 
Les pouvoirs et compétences au regard du projet OSM sont donc ventilés entre ces deux organes. De façon très schématique, le bureau aura à charge la gestion opérationnelle tandis que l’ensemble des membres sera en charge de la prise de décision ou de l’approbation concernant les grandes orientations du projet. 

###Exemple d’utilisation

En France, la ville de Brest et sa périphérie, dénommée Pays de Brest, ont été pionnières dans l’utilisation et la contribution au projet OSM. Ainsi, Brest Métropole Océane a décidé dès 2010 que les données géographiques qu'il détient seraient mises à disposition de tous, gracieusement et sans contrepartie, tant pour les particuliers que les acteurs économiques. Cela a notamment permis d’enrichir considérablement la base de données d’OpenStreetMap qui a ensuite été exploitée en retour par Brest Métropole Océane afin de mettre en valeur les services publics locaux (culture, services, etc.), le patrimoine et les services touristiques, mais également tous les services liés à Internet et au multimédia. 

Aller plus loin : 
* [Ressource 1](https://wiki.openstreetmap.org/wiki/Brest)
* [Ressource 2](http://www.wiki-brest.net/index.php/Portail:Cartes\_ouvertes)


